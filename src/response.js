
/* istanbul ignore next */
class Response {
    constructor(context, code = 200) {
        Object.assign(this, context);
        this.code = code;
    }

    status(code) {
        this.code = code;
        return this;
    }

    json(object) {
        this.body = object;
        return this;
    }

    end() {
        this.frozen = true;
    }
}

module.exports = Response;