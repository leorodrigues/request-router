
/* istanbul ignore next */
class Request {
    constructor(context) {
        Object.assign(this, context);
    }
}

module.exports = Request;