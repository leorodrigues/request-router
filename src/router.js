const RoutingKit = require('./routing-kit');

module.exports = class Router {
    constructor(routingKit, preHandlers, postHandlers, dispatchers) {

        /* istanbul ignore next */
        this.postHandlers = postHandlers || [ ];

        /* istanbul ignore next */
        this.preHandlers = preHandlers || [ ];

        /* istanbul ignore next */
        this.dispatchers = dispatchers || [ ];

        /* istanbul ignore next */
        this.routingKit = routingKit || new RoutingKit();
    }

    beforeEach(...handlers) {
        this.preHandlers.push(...handlers);
    }

    afterEach(...handlers) {
        this.postHandlers.unshift(...handlers);
    }

    delete(path, ...handlers) {
        this.dispatchers.push(
            this.routingKit.newDispatcher('delete', path, handlers));
    }

    post(path, ...handlers) {
        this.dispatchers.push(
            this.routingKit.newDispatcher('post', path, handlers));
    }

    put(path, ...handlers) {
        this.dispatchers.push(
            this.routingKit.newDispatcher('put', path, handlers));
    }

    get(path, ...handlers) {
        this.dispatchers.push(
            this.routingKit.newDispatcher('get', path, handlers));
    }

    async dispatch(input) {
        const inputRegexp = /^(quit|[\w-]+)?(\s*(\/.*))?$/;
        if (!inputRegexp.test(input)) return false;

        const [,method = 'get',, path] = inputRegexp.exec(input);
        if (method === 'quit') return true;

        const dispatcher = this.dispatchers.find(accepting(method, path));
        if (!dispatcher) return false;

        return dispatcher.dispatch(path, this.preHandlers, this.postHandlers);
    }
};

function accepting(method, path) {
    return d => d.accepts(method, path);
}
