const pathToRegexp = require('path-to-regexp');

class Dispatcher {
    constructor(method, pathTemplate, handlers, continuation, routingKit) {
        const keys = [ ];
        this.routeRegexp = pathToRegexp(pathTemplate, keys);
        this.continuation = continuation;
        this.routingKit = routingKit;
        this.handlers = handlers;
        this.method = method;
        this.keys = keys;
    }

    accepts(method, path) {
        return this.method === method && this.routeRegexp.test(path);
    }

    async dispatch(path, preHandlers, postHandlers) {
        const match = this.routeRegexp.exec(path);
        match.shift();
        const params = match.reduce(byKeyName(this.keys), { });

        const context = this.routingKit.newContext(params, this.method, path);
        const request = this.routingKit.newRequest(context);
        const response = this.routingKit.newResponse(context);

        const handlers = this.handlers;
        const stopRunning = await this.continuation.follow(
            request, response, ...preHandlers, ...handlers, ...postHandlers);

        disposeOfObject(request);
        disposeOfObject(response);

        return stopRunning;
    }
}

function byKeyName(keys) {
    return (p, c, i) => (p[keys[i].name] = c) && p;
}

function disposeOfObject(object) {
    for (const name in object)
        delete object[name];
}

module.exports = Dispatcher;