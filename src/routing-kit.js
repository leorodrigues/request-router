
const { Continuation } = require('@leorodrigues/continuation');

const Dispatcher = require('./dispatcher');
const Response = require('./response');
const Request = require('./request');

/* istanbul ignore next */
class RoutingKit {
    constructor(continuation) {
        this.continuation = continuation || new Continuation();
    }

    newDispatcher(method, path, handlers) {
        return new Dispatcher(method, path, handlers, this.continuation, this);
    }

    newResponse(context) {
        return new Response(context);
    }

    newRequest(context) {
        return new Request(context);
    }

    newContext(params, method, path) {
        return { params, method, path };
    }
}

module.exports = RoutingKit;