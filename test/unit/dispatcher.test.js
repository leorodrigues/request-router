
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;
chai.use(require('sinon-chai'));


const Dispatcher = require('../../src/dispatcher');

const sandbox = sinon.createSandbox();

const follow = sandbox.stub();
const newContext = sandbox.stub();
const newRequest = sandbox.stub();
const newResponse = sandbox.stub();

const continuation = { follow };

const routingKit = { newContext, newRequest, newResponse };

describe('Dispatcher', () => {
    describe('#accepts', () => {
        it('Should accept if both method and route match', () => {
            const subject = new Dispatcher('GET', '/person/:id', [ ], continuation, routingKit);
            expect(subject.accepts('GET', '/person/42')).to.be.true;
        });
        it('Should reject if the method is different', () => {
            const subject = new Dispatcher('GET', '/person/:id', [ ], continuation, routingKit);
            expect(subject.accepts('POST', '/person/42')).to.be.false;
        });
        it('Should reject if the route does not match', () => {
            const subject = new Dispatcher('GET', '/person/:id', [ ], continuation, routingKit);
            expect(subject.accepts('GET', '/customer/42')).to.be.false;
        });
    });

    describe('#dispatch', () => {
        it('Should invoke all handlers', async () => {
            newContext.returns('context');
            newRequest.returns('request');
            newResponse.returns('response');

            const subject = new Dispatcher(
                'GET', '/person/:id', ['during'], continuation, routingKit);

            await subject.dispatch('/person/19', ['before'], ['after']);

            expect(newRequest).to.be.calledOnceWithExactly('context');
            expect(newResponse).to.be.calledOnceWithExactly('context');
            expect(newContext).to.be.calledOnceWithExactly(
                sinon.match({ id: '19' }), 'GET', '/person/19');
            expect(follow).to.be.calledOnceWithExactly(
                'request', 'response', 'before', 'during', 'after');
        });
    });
});