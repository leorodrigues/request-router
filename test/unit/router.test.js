const chai = require('chai');
const { expect } = chai;

const sinon = require('sinon');
chai.use(require('sinon-chai'));

const Router = require('../../src/router');

const sandbox = sinon.createSandbox();

const newDispatcher = sandbox.stub();

const accepts = sandbox.stub();
const dispatch = sandbox.stub();

describe('Router', () => {
    afterEach(() => sandbox.reset());

    describe('#beforeEach', () => {
        it('Should append global handlers', () => {
            const subject = new Router({ }, ['x']);
            subject.beforeEach('a', 'b', 'c');
            expect(subject.preHandlers).to.be.deep.equal(['x', 'a', 'b', 'c' ]);
        });
    });
    describe('#afterEach', () => {
        it('Should prepend global handlers', () => {
            const subject = new Router({ }, [ ], ['x']);
            subject.afterEach('a', 'b', 'c');
            expect(subject.postHandlers).to.be.deep.equal([ 'a', 'b', 'c', 'x' ]);
        });
    });
    describe('#delete', () => {
        it('Should instanciate a dispatcher', () => {
            newDispatcher.returns('dispatcher');
            const subject = new Router({ newDispatcher });
            subject.delete('path', 'a', 'b', 'c');
            expect(subject.dispatchers).to.be.deep.equal(['dispatcher']);
            expect(newDispatcher).to.be.calledOnceWithExactly('delete', 'path', ['a', 'b', 'c']);
        });
    });
    describe('#post', () => {
        it('Should instanciate a dispatcher', () => {
            newDispatcher.returns('dispatcher');
            const subject = new Router({ newDispatcher });
            subject.post('path', 'a', 'b', 'c');
            expect(subject.dispatchers).to.be.deep.equal(['dispatcher']);
            expect(newDispatcher).to.be.calledOnceWithExactly('post', 'path', ['a', 'b', 'c']);
        });
    });
    describe('#put', () => {
        it('Should instanciate a dispatcher', () => {
            newDispatcher.returns('dispatcher');
            const subject = new Router({ newDispatcher });
            subject.put('path', 'a', 'b', 'c');
            expect(subject.dispatchers).to.be.deep.equal(['dispatcher']);
            expect(newDispatcher).to.be.calledOnceWithExactly('put', 'path', ['a', 'b', 'c']);
        });
    });
    describe('#get', () => {
        it('Should instanciate a dispatcher', () => {
            newDispatcher.returns('dispatcher');
            const subject = new Router({ newDispatcher });
            subject.get('path', 'a', 'b', 'c');
            expect(subject.dispatchers).to.be.deep.equal(['dispatcher']);
            expect(newDispatcher).to.be.calledOnceWithExactly('get', 'path', ['a', 'b', 'c']);
        });
    });
    describe('#dispatch', () => {
        it('Should signal to continue if the input is unrecognized', async () => {
            const subject = new Router({ newDispatcher });
            expect(await subject.dispatch('some random string')).to.be.false;
        });

        it('Should signal to continue if there is no matching dispatcher', async () => {
            const subject = new Router({ newDispatcher });
            expect(await subject.dispatch('get /person/x')).to.be.false;
        });

        it('Should signal to halt if the input is "quit"', async () => {
            const subject = new Router({ newDispatcher });
            expect(await subject.dispatch('quit')).to.be.true;
        });

        it('Should invoke a dispatcher if there is one', async () => {
            accepts.returns(true);
            dispatch.returns(true);

            const subject = new Router({ }, [ 'a' ], [ 'b' ], [ { accepts, dispatch } ]);

            expect(await subject.dispatch('put /person/x')).to.be.true;

            expect(accepts).to.be.calledOnceWithExactly('put', '/person/x');
            expect(dispatch).to.be.calledOnceWithExactly('/person/x', [ 'a' ], [ 'b' ]);
        });

        it('Should invoke a dispatcher with "get" if the method is ommited', async () => {
            accepts.returns(true);
            dispatch.returns(true);

            const subject = new Router({ }, [ 'a' ], [ 'b' ], [ { accepts, dispatch } ]);

            expect(await subject.dispatch('/person/x')).to.be.true;

            expect(accepts).to.be.calledOnceWithExactly('get', '/person/x');
            expect(dispatch).to.be.calledOnceWithExactly('/person/x', [ 'a' ], [ 'b' ]);
        });
    });
});