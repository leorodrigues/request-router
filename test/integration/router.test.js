const chai = require('chai');
const { expect } = chai;

const { Router } = require('../../');

describe('Router', () => {
    it('Should carry out the request', async () => {
        const subject = new Router();

        const initializeData = (request, response, next) => {
            request.data = [ 'k' ];
            next();
        };

        const finalizeData = (request, response, next) => {
            request.data.push('d');
            response.json(request.data);
            response.end();
            next();
        };

        const createCustomer = (request, response, next) => {
            request.data.push('created');
            next();
        };

        const saveCustomer = (request, response, next) => {
            request.data.push('saved');
            next();
        };

        const confirmExpectations = (request, response) => {
            expect(response.body).to.be.deep.equal([ 'k', 'created', 'saved', 'd' ]);
        };

        subject.beforeEach(initializeData);

        subject.afterEach(finalizeData, confirmExpectations);

        subject.post('/customer/:id', createCustomer, saveCustomer);

        await subject.dispatch('post /customer/11');
    });
});